    var btn = document.getElementById("btn");
    var resultado = document.getElementById("resultado");
    var operacion = document.getElementById("operacion");
    var inputUno = document.getElementById("input-uno");
    var inputDos = document.getElementById("input-dos");   

    btn.addEventListener("click",function(){
        var n1 = inputUno.value;
        var n2 = inputDos.value;
        resultado.style.color = "#000000";

        if(elementoVacio(n1,n2)){
            resultado.style.color = "red";
            resultado.innerHTML = "Debe ingresar ambos números.";
        }else if(!esEntero(n1) || !esEntero(n2)){
            resultado.style.color = "red";
            resultado.innerHTML = "Los elementos ingresados deben ser enteros";
        }else{
            switch(operacion.value){
                case "sumar":resultado.innerHTML = suma(n1,n2);
                    break;
                case "restar":resultado.innerHTML = resta(n1,n2);
                    break;
                case "multiplicar":resultado.innerHTML = multiplicacion(n1,n2);
                    break;
                case "dividir":
                    if(n2>0){
                        resultado.innerHTML = division(n1,n2);
                    }else{
                        resultado.style.color = "red";
                        resultado.innerHTML = "El divisor debe ser mayor a 0";                 
                    }
                    break;
            }
        }

 
    });

    function suma(n1,n2){
        return parseInt(n1) + parseInt(n2);
    }

    function resta(n1,n2){
        return parseInt(n1) - parseInt(n2);
    }

    function multiplicacion(n1,n2){
        return parseInt(n1) * parseInt(n2);
    }

    function division(n1,n2){
        return parseInt(n1) / parseInt(n2);
    }

    function elementoVacio(n1,n2){
        if(!n1 | !n2){
            return true;
        }
    }

    function esEntero(n1){
        return !isNaN(n1) && parseInt(Number(n1)) == n1 && !isNaN(parseInt(n1,10));
    }