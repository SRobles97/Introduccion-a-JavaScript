    var btn = document.getElementById("btn");
    var resultado = document.getElementById("resultado");
    var inputUno = document.getElementById("input-uno");
    var inputDos = document.getElementById("input-dos");   

    btn.addEventListener("click",function(){
        var n1 = inputUno.value;
        var n2 = inputDos.value;

        resultado.innerHTML = generarNumeroAleatorio(n1,n2);
    });

    function generarNumeroAleatorio(n1,n2){
        return Math.floor(Math.random() * (parseInt(n2)-parseInt(n1) + 1)) + parseInt(n1);
    }
    